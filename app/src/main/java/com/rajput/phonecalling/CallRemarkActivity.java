package com.rajput.phonecalling;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.rajput.phonecalling.fragment.CallRemarkFragment;

public class CallRemarkActivity extends AppCompatActivity {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_remark);
        showMainFragment();
    }

    private void showMainFragment() {
        getFragmentManager().beginTransaction()
                .add(R.id.call_remark_container, CallRemarkFragment.newInstance())
                .commit();
    }


}
