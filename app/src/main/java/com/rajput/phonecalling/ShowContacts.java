package com.rajput.phonecalling;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;

import com.rajput.phonecalling.listener.IncomingListener;
import com.rajput.phonecalling.model.UserContact;

import java.util.ArrayList;
import java.util.List;


public class ShowContacts extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener,
    IncomingListener{
    /*private BroadcastReceiver myBroadcastReceiver = new PhoneBroadcastReceiver();*/

    public static final String PHONE_NUMBER_TAG = "PhoneNumber";
    boolean isDisconnected = false;
    PhoneBroadcastReceiver phoneBroadcastReceiver = new PhoneBroadcastReceiver();

    private ListView userAccountListView = null;
    private TextView userAccountListEmptyTextView = null;
    private ListView test = null;
    List<UserContact> contactList;
    private String phoneNumber = "";

    public static ShowContacts newInstance() {
        return new ShowContacts();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_user_account_list_view, container, false);
        test = (ListView) view.findViewById(R.id.user_account_list_view);
        test.setOnItemClickListener(this);
        PhoneBroadcastReceiver.bindListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        /*final IntentFilter intentFilter = new IntentFilter("android.intent.action.PHONE_STATE");
        getActivity().registerReceiver(myBroadcastReceiver, intentFilter);*/
        userAccountListView = (ListView)view.findViewById(R.id.user_account_list_view);
        userAccountListEmptyTextView = (TextView)view.findViewById(R.id.user_account_list_empty_text_view);
        userAccountListView.setEmptyView(userAccountListEmptyTextView);
        contactList = getAllContactsList();
        if(contactList.size() > 0) {
            MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getActivity(), contactList);
            userAccountListView.setAdapter(adapter);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contact_detail: {
                /*Fragment fragment = new tasks();
                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();*/
                break;
            }
        }
        Toast.makeText(getActivity(), ""+view.getId(), Toast.LENGTH_SHORT).show();
    }

    private List<UserContact> getAllContactsList() {
        UserContact contact = null;
        List<UserContact> userContacts = new ArrayList<UserContact>();

//        ''''''''''''''''''''''''''''''''''''''''''''''''
        contact = new UserContact();
        contact.setName("A");
        contact.setNumber("7014967427");
        userContacts.add(contact);


        contact = new UserContact();
        contact.setName("B");
        contact.setNumber("8740914692");
        userContacts.add(contact);

//        ''''''''''''''''''''''''''''''''''''''''''''''''
        /*ArrayList<String> nameList = new ArrayList<>();
        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                contact = new UserContact();
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));
                contact.setName(name);
                nameList.add(name);
                if (cur.getInt(cur.getColumnIndex( ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contact.setNumber(phoneNo);
                    }
                    pCur.close();
                }
                userContacts.add(contact);
            }
        }
        if (cur != null) {
            cur.close();
        }*/
        return userContacts;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        UserContact contact = contactList.get(position);
        phoneNumber = contact.getNumber();
        callOnNumber(phoneNumber);
    }

    void callOnNumber(String number) {
            Intent phoneIntent = new Intent(Intent.ACTION_CALL);
            phoneIntent.setData(Uri.parse("tel:" + number));
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(phoneIntent);
    }


    @Override
    public void onCallDisconnect(boolean isDisconnected) {
        if(isDisconnected) {
            Log.d("ShowContacts", "onCallDisconnect");
            Intent callIntent = new Intent(getActivity(), CallRemarkActivity.class);
            callIntent.putExtra(PHONE_NUMBER_TAG, phoneNumber);
            Log.d("ShowContacts", "Intent:// "+callIntent.toString());
            startActivity(callIntent);
            this.isDisconnected = true;
        }
    }

   /* @Override
    public void onStart() {
        Log.d("ShowContacts", "onStart");
        super.onStart();
    }

    @Override
    public void onStop() {
        Log.d("ShowContacts", "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.d("ShowContacts", "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onResume() {
        Log.d("ShowContacts", "onResume");
        super.onResume();
        IntentFilter filter = new IntentFilter("android.intent.action.PHONE_STATE");
        getActivity().registerReceiver(phoneBroadcastReceiver, filter);
    }

    @Override
    public void onPause() {
        Log.d("ShowContacts", "onPause");
        super.onPause();
        getActivity().unregisterReceiver(phoneBroadcastReceiver);
    }*/


    /*@Override
    public void onResume()
    {
        super.onResume();
        if(isDisconnected) {
            isDisconnected = false;
            Intent callIntent = new Intent(getActivity(), CallRemarkActivity.class);
            callIntent.putExtra(PHONE_NUMBER_TAG, phoneNumber);
            startActivity(callIntent);
        }
    }*/
}
