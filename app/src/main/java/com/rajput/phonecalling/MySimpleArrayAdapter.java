package com.rajput.phonecalling;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rajput.phonecalling.model.UserContact;

import java.util.List;

public class MySimpleArrayAdapter extends ArrayAdapter<UserContact> {
    private final Context context;
    private final List<UserContact> userContact;

    public MySimpleArrayAdapter(Context context, List<UserContact> userContact) {
        super(context, R.layout.activity_user_account_list_view_item, userContact);
        this.context = context;
        this.userContact = userContact;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_user_account_list_view_item, parent, false);
        TextView name = (TextView) rowView.findViewById(R.id.name);
        TextView number = (TextView) rowView.findViewById(R.id.number);
        name.setText(userContact.get(position).getName());
        number.setText(userContact.get(position).getNumber());

        return rowView;
    }
}