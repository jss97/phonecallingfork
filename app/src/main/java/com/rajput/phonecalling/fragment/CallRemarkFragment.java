package com.rajput.phonecalling.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rajput.phonecalling.CallRemarkActivity;
import com.rajput.phonecalling.MainActivity;
import com.rajput.phonecalling.R;
import com.rajput.phonecalling.ShowContacts;
import com.rajput.phonecalling.model.CallRemark;

import java.util.HashMap;
import java.util.Map;


public class CallRemarkFragment extends Fragment implements View.OnClickListener {

    FirebaseDatabase database;
    DatabaseReference mDatabase;
    String number;
    EditText remark;
    Button submit;


    public static CallRemarkFragment newInstance() {
        return new CallRemarkFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_fragment, container, false);
        remark = view.findViewById(R.id.remark_edittext);
        submit = view.findViewById(R.id.submit_remark);
        submit.setOnClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this.number = getActivity().getIntent().getExtras().getString(ShowContacts.PHONE_NUMBER_TAG);
    }

    void addCallRemark() {
        database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference().child("CallRemark");
        String rid = mDatabase.push().getKey();
        CallRemark callRemark = new CallRemark(rid, number, remark.getText().toString());
        Map<String, Object> postValues = callRemark.toMap();
        mDatabase.child(rid).updateChildren(postValues)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getActivity(), "Remark Saved", Toast.LENGTH_SHORT).show();
                        Intent callIntent = new Intent(getActivity(), MainActivity.class);
                        startActivity(callIntent);
                        getActivity().finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Error to save Remark", Toast.LENGTH_SHORT).show();
                        Log.e("SAVE_REMARK", e.getMessage());
                    }
                });
        ;
    }

    @Override
    public void onClick(View view) {
        addCallRemark();
    }
}
