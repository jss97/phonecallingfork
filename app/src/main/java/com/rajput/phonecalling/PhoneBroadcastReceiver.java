package com.rajput.phonecalling;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.rajput.phonecalling.listener.IncomingListener;

import java.util.Date;


public class PhoneBroadcastReceiver extends BroadcastReceiver{
    private static IncomingListener iListener;

    /*@Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String state = extras.getString("android.intent.action.PHONE_STATE");
            Log.w("MY_DEBUG_TAG", "JSS????????????????");
            *//*if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                String phoneNumber = extras
                        .getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                onCallStateChanged(context, state, phoneNumber);
                Log.w("MY_DEBUG_TAG", phoneNumber);
            }*//*
        }
    }*/

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.w("PhoneReceiving", "Listener:// " + iListener.toString());

        Log.d("PhoneReceiving", ""+intent.getAction());
        if(intent.getAction() != null && intent.getAction().equals("android.intent.action.PHONE_STATE")) {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

            if(state != null) {
                Log.w("PhoneReceiving", "onCallStateChanged called");
                onCallStateChanged(context, state, incomingNumber);
            }
            Log.d("PhoneReceiving State", ""+state);
        }
        if(intent.getAction() != null && intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
        }
    }

    public static void bindListener(IncomingListener listener){
        iListener = listener;
    }


    public void onCallStateChanged(Context context, String state, String number) {
        Log.w("PhoneReceiving", "in onCallStateChanged method");
        switch (state) {
            case "RINGING":
                break;
            case "IDLE":
                iListener.onCallDisconnect(true);
                break;
            case "OFFHOOK":
                break;
        }
    }
}
