package com.rajput.phonecalling;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;


public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_READ_CONTACTS = 79;
    public static final int REQUEST_PHONE_CALL = 80;
    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 81;
    PhoneBroadcastReceiver phoneBroadcastReceiver = new PhoneBroadcastReceiver();


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
        }
        checkPermission();
    }

    void checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            requestReadContactPermission();
        } else if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestCallPhonePermission();
        } else if (getApplicationContext().checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
             showMainFragment();
        }
    }

    private void showMainFragment() {
        getFragmentManager().beginTransaction()
                .add(R.id.container, ShowContacts.newInstance())
                .commit();
    }

    private void requestReadContactPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS},
                REQUEST_READ_CONTACTS);
    }

    private void requestCallPhonePermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},
                REQUEST_PHONE_CALL);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            checkPermission();
        } else {
            Toast.makeText(this, "You have disabled a required permission", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity", "onDestroy");
        this.unregisterReceiver(phoneBroadcastReceiver);
    }

    @Override
    public void onResume() {
        Log.d("MainActivity", "onResume");
        super.onResume();
        IntentFilter filter = new IntentFilter("android.intent.action.PHONE_STATE");
        this.registerReceiver(phoneBroadcastReceiver, filter);
    }
}
